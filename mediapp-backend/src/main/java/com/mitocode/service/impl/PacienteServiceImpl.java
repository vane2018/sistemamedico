package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IPacienteDAO;
import com.mitocode.model.Paciente;
import com.mitocode.service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService{

	@Autowired
	private IPacienteDAO dao;
	//implemento los metodos haciendo una inyeccion de dependencia a la capa del dao 
	//por medio del autowired para disponer del guardar,modificar,listar...etc
	@Override
	public Paciente registrar(Paciente paciente) {
		
		return dao.save(paciente);
	}

	@Override
	public Paciente modificar(Paciente paciente) {
		// TODO Auto-generated method stub
		return  dao.save(paciente);
	}

	@Override
	public void eliminar(int idPaciente) {
		// TODO Auto-generated method stub
		dao.delete(idPaciente);
	}

	@Override
	public Paciente listarId(int idPaciente) {
		// TODO Auto-generated method stub
		return dao.findOne(idPaciente);
	}

	@Override
	public List<Paciente> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public Page<Paciente> listarPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return dao.findAll(pageable);
	}

}
