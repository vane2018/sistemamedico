package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="examen")
public class Examen {
	
		
		
		@Id
		//el identity en posgre se comporta como "el mismo motor genera una secuencia"
		@GeneratedValue(strategy= GenerationType.IDENTITY)
		private int idExamen;
	    //@column me permite definir nombre,longitud del elemento
		@Column(name="nombres", nullable=false, length=50)
		private String nombres;
		
		@Column(name="descripcion", nullable=false, length=250)
	    private String descripcion;

		public int getIdExamen() {
			return idExamen;
		}

		public void setIdExamen(int idExamen) {
			this.idExamen = idExamen;
		}

		public String getNombres() {
			return nombres;
		}

		public void setNombres(String nombres) {
			this.nombres = nombres;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		
}
