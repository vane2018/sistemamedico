package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/*indicacmos que esta siendo trabajado por jpa
 * Entity puede tomar valores por defecto o no*/
	@ApiModel(description= "Informacion del paciente")
@Entity
	//si no le indico nada le estoy diciendo que tome el nombre de la classe Paciente
    //from Paciente---nombre de la clase el cual le indico que es una entidad
@Table(name="paciente")
//es el nombre que la tabla va a adoptar cuando se cree el esquema
public class Paciente {
	//FROM "nombre de la clase" l cual le indico que es una
	//entidad de bd, el nonmbre de la clase es Paciente entonces es valido (FROM Paciente)
	@Id
	//el identity en posgre se comporta como "el mismo motor genera una secuencia"
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idPaciente;
    //@column me permite definir nombre,longitud del elemento
	
	@ApiModelProperty(notes= "Nombres debe tener minimo 3 caracteres")
	@Size(min= 3, message="Nombres debe tener minimo 3 caracteres")
	@Column(name="nombres", nullable=true, length=70)
	private String nombres;
	
	
	
	@ApiModelProperty(notes= "Apellidoss debe tener minimo 3 caracteres")
	@Size(min= 3, message="Nombres debe tener minimo 3 caracteres")
	@Column(name="apellidos", nullable=true, length=70)
    private String apellidos;
	
	@ApiModelProperty(notes= "dni debe tener minimo 8 caracteres")
	@Size(min= 8,max=8, message="Nombres debe tener minimo 3 caracteres")
    @Column(name="dni",nullable=false,length=10)
    private String dni;
    
	
	@ApiModelProperty(notes= "direccion debe tener minimo 3 caracteres")
	@Size(min= 3, message="Nombres debe tener minimo 3 caracteres")
    @Column (name="direccion",length=150)
    private String direccion;
    
	@ApiModelProperty(notes= "Telefono debe tener 9 caracteres")
	@Size(min= 9, max=9, message="Telefon debe tener 9 caracteres")
    @Column(name="telefono",length=9)
    private String telefono;

	public int getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
    
    
}
