package com.mitocode.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="signo")
public class Signos {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idSigno;
	
	@ManyToOne
	@JoinColumn(name="id_paciente",nullable=false)
	private Paciente paciente;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime fecha;
	
	@ApiModelProperty(notes= "temperatura debe tener minimo 3 caracteres")
	@Size(min= 3, message="temperatura debe tener minimo 3 caracteres")
@Column(name="temperatura",nullable=false,length=10)
private String temperatura;



@ApiModelProperty(notes= "pulso debe tener minimo 3 caracteres")
@Size(min= 3, message="pulso debe tener minimo 3 caracteres")
@Column (name="pulso",length=20)
private String pulso;


@ApiModelProperty(notes= "pulso debe tener minimo 3 caracteres")
@Size(min= 3, message="pulso debe tener minimo 3 caracteres")
@Column (name="ritmo",length=20)
private String ritmo;
	
	 public int getIdSignos() {
		return idSigno;
	}


	public void setIdSignos(int idSigno) {
		this.idSigno = idSigno;
	}


	public Paciente getPaciente() {
		return paciente;
	}


	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}


	public LocalDateTime getFecha() {
		return fecha;
	}


	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}


	public String getTemperatura() {
		return temperatura;
	}


	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}


	public String getPulso() {
		return pulso;
	}


	public void setPulso(String pulso) {
		this.pulso = pulso;
	}


	public String getRitmo() {
		return ritmo;
	}


	public void setRitmo(String ritmo) {
		this.ritmo = ritmo;
	}


	

}
