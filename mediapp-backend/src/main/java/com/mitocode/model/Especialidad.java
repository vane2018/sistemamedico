package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="especialidad")
public class Especialidad {

	@Id
	//el identity en posgre se comporta como "el mismo motor genera una secuencia"
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idEspecialidad;
	
    //@column me permite definir nombre,longitud del elemento
	@Column(name="nombre", nullable=false, length=50)
	private String nombre;

	public int getIdEspecialidad() {
		return idEspecialidad;
	}

	public void setIdEspecialidad(int idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
