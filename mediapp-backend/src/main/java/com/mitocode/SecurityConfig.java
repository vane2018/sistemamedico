package com.mitocode;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;


@Configuration//para que spring detecte esta clase como una clase de configuracion (donde hay definiciones de binds)para que yo pueda instanciar binds
//voy a instanciar bind mas adelante
@EnableWebSecurity//por que stoy trabajando con sprint security para temas de servicios web, para la parte mvc
@EnableGlobalMethodSecurity(prePostEnabled = true)//para que toda mi aplicacion este protegida con sprint security,para todas las petisiones
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	//estos tres vbles se estan poblando con la informacion  que tengo definido en aplication property
	@Value("${security.signing-key}")
	private String signingKey;

	@Value("${security.encoding-strength}")
	private Integer encodingStrength;

	@Value("${security.security-realm}")
	private String securityRealm;
	
	@Autowired
	private DataSource dataSource;
	
	//acceso a la bd para obtener los datos,informacion de usuarios y roles
	@Autowired	
	private UserDetailsService userDetailsService;
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder= new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}
	
	@Bean//internamente se esta definiendo una clase en su cor conteiner , para que mas adelante otra clase la pueda
	//inyectar, le haga un autowired
	
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Autowired//al realizar el autowired estoy inyectando,por contructor,ya puedo reconocerlo en memoria, por que previamente
	//fue un @bind
	public void configure(AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(userDetailsService).passwordEncoder(bcrypt);//me dice donde se encuentra la tabla con ususario y roles
	}
	
	@Override//significa que estoy sobrescribiendo los metodos
	protected void configure(HttpSecurity http) throws Exception {
		http		
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .httpBasic()
        .realmName(securityRealm)
        .and()
        .csrf()
        .disable();        
	}
	
	
	//con esto se genera el token
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {//aqui estoy definiendo el convertor para que se 
		//se generen los tokens
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(signingKey);//cojo las vbles		
		return converter;
	}
	
	@Bean
	public TokenStore tokenStore() {
		//return new JwtTokenStore(accessTokenConverter());
		// JwtokenStore: es una vble en memoria que contiene todos los token que se generan en la aplicacion
		//todos los usuarios que en angula  se quieran conectar a la aplicacion el back en los proveera un token que se al macenaran en esta vble
	
	//------clase 12-------
		return new JdbcTokenStore(this.dataSource);
	//almacenamos en BD,es el termino que se utiliza para refenciar BD
	
	}
	
	
	
	//con esto guardo el token en memoria
	@Bean
	@Primary//PRIMERO NECESITA ESTA CREADO ANTES DE SER INSTANCIADOS en el autowired(en el resourseService)
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore());//donde se esta almacenando el token
		defaultTokenServices.setSupportRefreshToken(true);	//el token es refrescado
		defaultTokenServices.setReuseRefreshToken(false);	
		return defaultTokenServices;
	}
	
}
