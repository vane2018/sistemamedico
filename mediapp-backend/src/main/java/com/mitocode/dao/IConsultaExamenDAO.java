package com.mitocode.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.model.ConsultaExamen;

@Repository
public interface IConsultaExamenDAO extends JpaRepository<ConsultaExamen, Integer> {
	//este es un query de tipo DML, porque hace manipulacion de datos insert,update
	//@Transactional
	//confirma una transaccion,como tengo una sola consulta va a confirmar esa consulta,
	//cuando tenga un bloque va a confirmar ese bloque
	@Modifying
	//esta notacion es para dml, indica que el query va a ser una modificacion(insercion,etc), va a quedar como temporal y hay que confirmara la modificacion con transaccional ,si es selec no se requiere de esto
	@Query(value = "INSERT INTO consulta_examen(id_consulta, id_examen) VALUES (:idConsulta, :idExamen)", nativeQuery = true)
	int registrar(@Param("idConsulta") Integer idConsulta,@Param("idExamen") Integer idExamen);

	@Query("from ConsultaExamen ce where ce.consulta.idConsulta=:idConsulta")
	List<ConsultaExamen> listarExamenesPorConsulta(@Param("idConsulta") Integer idconsulta);

}
