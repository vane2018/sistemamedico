package com.mitocode.controller;

import javax.annotation.Resource;

import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuarios")
public class UserController {

	@Resource(name = "tokenServices")
	private ConsumerTokenServices tokenServices;
	//internamente hay una clase consumerTokenServices,es propia  de sprint oaut,es un servicio que dispone realizar 
	//un servicio con respecto al token

	@GetMapping(value = "/anular/{tokenId:.*}")
	//el token tiene un formato muy extenso por eso pongo :.* esto considera todo lo que venga por parametro,
	//todo lo que venga despues del slash
	public void revokeToken(@PathVariable("tokenId") String token) {
		tokenServices.revokeToken(token);	//metodo propio de sprint oaut 	
		
	}
}
