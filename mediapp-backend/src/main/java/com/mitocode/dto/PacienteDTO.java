package com.mitocode.dto;

import org.springframework.hateoas.ResourceSupport;
//los DTO son objetos de apoyo para cumplir el objetivo de que la clase paciente no herede de dos clases
public class PacienteDTO extends ResourceSupport {
	private int idPaciente;
	private String nombresCompletos;

	public int getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNombresCompletos() {
		return nombresCompletos;
	}

	public void setNombresCompletos(String nombresCompletos) {
		this.nombresCompletos = nombresCompletos;
	}


}
