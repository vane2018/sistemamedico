package com.mitocode.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
//estoy anotando para q sea reconocida 
//en una peticion de tipo rest(es el tipo de respuesta not_found)
public class ModeloNotFoundException  extends RuntimeException{
	
	public ModeloNotFoundException(String mensaje) {
		super(mensaje);
	}

}
/*esto me sirve si por ejemplo ingreso un paciente que no existe me muestra
 un mensaje "no encontrado"*/
 